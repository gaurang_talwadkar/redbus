//
//  WelcomeScreen.swift
//  Redbus
//
//  Created by Gaurang Talwadkar on 14/03/16.
//  Copyright © 2016 Gaurang. All rights reserved.
//

import UIKit

class WelcomeScreen: UIViewController {

    let spaceBetweenLetters: CGFloat = 6
    var constraintsArray: NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        constraintsArray = NSArray()
        
        self.addLetterLabels()
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
            self.animateLetters()
        }
    }
    
    func addLetterLabels() {
        let rLabel = addLabel("R")
        let eLabel = addLabel("E")
        let dLabel = addLabel("D")
        let bLabel = addLabel("B")
        let uLabel = addLabel("U")
        let sLabel = addLabel("S")
        
        self.view.layoutIfNeeded()
        
        self.view.translatesAutoresizingMaskIntoConstraints = false
        rLabel.translatesAutoresizingMaskIntoConstraints = false
        eLabel.translatesAutoresizingMaskIntoConstraints = false
        dLabel.translatesAutoresizingMaskIntoConstraints = false
        bLabel.translatesAutoresizingMaskIntoConstraints = false
        uLabel.translatesAutoresizingMaskIntoConstraints = false
        sLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let dLabelXLayoutConstraint = centerXConstraintForLabel(dLabel, constant: -(spaceBetweenLetters/2 + CGRectGetMidX(dLabel.frame)))
        let bLabelXLayoutConstraint = centerXConstraintForLabel(bLabel, constant: spaceBetweenLetters/2 + CGRectGetMidX(dLabel.frame))
        
        let rLabelXLayoutConstraint = NSLayoutConstraint(item: rLabel, attribute: .Trailing, relatedBy: .Equal, toItem: eLabel, attribute: .Leading, multiplier: 1.0, constant: -spaceBetweenLetters)
        let eLabelXLayoutConstraint = NSLayoutConstraint(item: eLabel, attribute: .Trailing, relatedBy: .Equal, toItem: dLabel, attribute: .Leading, multiplier: 1.0, constant: -spaceBetweenLetters)
        let uLabelXLayoutConstraint = NSLayoutConstraint(item: uLabel, attribute: .Leading, relatedBy: .Equal, toItem: bLabel, attribute: .Trailing, multiplier: 1.0, constant: spaceBetweenLetters)
        let sLabelXLayoutConstraint = NSLayoutConstraint(item: sLabel, attribute: .Leading, relatedBy: .Equal, toItem: uLabel, attribute: .Trailing, multiplier: 1.0, constant: spaceBetweenLetters)
        
        let rLabelYLayoutConstraint = centerYConstraintForLabel(rLabel)
        let eLabelYLayoutConstraint = centerYConstraintForLabel(eLabel)
        let dLabelYLayoutConstraint = centerYConstraintForLabel(dLabel)
        let bLabelYLayoutConstraint = centerYConstraintForLabel(bLabel)
        let uLabelYLayoutConstraint = centerYConstraintForLabel(uLabel)
        let sLabelYLayoutConstraint = centerYConstraintForLabel(sLabel)
        
        self.view.addConstraints([rLabelXLayoutConstraint, rLabelYLayoutConstraint, eLabelXLayoutConstraint, eLabelYLayoutConstraint, dLabelXLayoutConstraint, dLabelYLayoutConstraint, bLabelXLayoutConstraint, bLabelYLayoutConstraint, uLabelXLayoutConstraint, uLabelYLayoutConstraint, sLabelXLayoutConstraint, sLabelYLayoutConstraint])
        
        self.view.updateConstraints()
        
        constraintsArray = [rLabelYLayoutConstraint, eLabelYLayoutConstraint, dLabelYLayoutConstraint, bLabelYLayoutConstraint, uLabelYLayoutConstraint, sLabelYLayoutConstraint]
    }
    
    func addLabel(letter: String) -> UILabel {
        let label = UILabel()
        label.text = letter
        label.textColor = UIColor.redColor()
        label.sizeToFit()
        
        self.view.addSubview(label)
        
        return label
    }
    
    func centerXConstraintForLabel(label: UILabel, constant: CGFloat) -> NSLayoutConstraint {
        let centerXLayoutConstraint = NSLayoutConstraint(item: label, attribute: .CenterX, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1.0, constant: constant)
        
        return centerXLayoutConstraint
    }
    
    func centerYConstraintForLabel(label: UILabel) -> NSLayoutConstraint {
        let midYValue = CGRectGetMidY(self.view.frame)
        let centerYLayoutConstraint = NSLayoutConstraint(item: label, attribute: .CenterY, relatedBy: .Equal, toItem: self.view, attribute: .CenterY, multiplier: 1.0, constant: -(midYValue + 20))
        
        return centerYLayoutConstraint
    }
    
    func animateLetters() {
        self.view.layoutIfNeeded()
        
        constraintsArray.enumerateObjectsUsingBlock { (constraint, index, stop) -> Void in
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(index) * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
                UIView.animateWithDuration(1) { () -> Void in
                    (constraint as! NSLayoutConstraint).constant = 0
                    
                    self.view.layoutIfNeeded()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
